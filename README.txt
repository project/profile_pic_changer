INTRODUCTION
------------
The Profile picture changer module allows users to change their
profile picture through a modal form directly on their profile page,
without having to go to the profile edit form.


REQUIREMENTS
------------
This module requires the following modules:

 * CTools (https://drupal.org/project/ctools)


RECOMMENDED MODULES
-------------------
 * Jquery plugins (https://www.drupal.org/project/jquery_plugin):
   When enabled, a nice tooltip will be displayed in the user picture
   when the user hovers the mouse over it.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Make sure you create a default picture and that it is being
   displayed on user profile pages of users without a picture,
   otherwise they will not be able to upload their first image.


CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no
configuration. When this module enabled, user profile pictures will be
editable using a modal dialog.
